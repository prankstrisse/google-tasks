package com.google.tasks.api;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.tasks.Tasks;
import com.google.api.services.tasks.TasksScopes;
import com.google.api.services.tasks.model.Task;
import com.google.api.services.tasks.model.TaskList;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Test {

    private static final String APPLICATION_NAME = "A sample app";

    /**
     * Directory to store user credentials.
     */
    private static final java.io.File DATA_STORE_DIR =
            new java.io.File(System.getProperty("user.home"), ".store/tasks_sample");

    /**
     * Global instance of the {@link com.google.api.client.util.store.DataStoreFactory}.
     * The best practice is to make it a single globally shared instance across your application.
     */
    private static FileDataStoreFactory dataStoreFactory;

    /**
     * Global instance of the JSON factory.
     */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    /**
     * Global instance of the HTTP transport.
     */
    private static HttpTransport httpTransport;

    @SuppressWarnings("unused")
    protected static Tasks client;

    /**
     * Authorizes the installed application to access user's protected data.
     */
    private static Credential authorize() throws Exception {
        // load client secrets
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
                new InputStreamReader(Test.class.getResourceAsStream("/client_secrets.json")));
        if (clientSecrets.getDetails().getClientId().startsWith("Enter") ||
                clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
            System.out.println(
                    "Overwrite the src/main/resources/client_secrets.json file with the client secrets file "
                            + "you downloaded from the Quickstart tool or manually enter your Client ID and Secret "
                            + "from https://code.google.com/apis/console/?com.com.gTasks.com.google.tasks.com.google.tasks.apiasdf=tasks#project:553851345788 "
                            + "into src/main/resources/client_secrets.json");
            System.exit(1);
        }

        // Set up authorization code flow.
        Set<String> scopes = new HashSet<String>();
        scopes.add(TasksScopes.TASKS);
        scopes.add(TasksScopes.TASKS_READONLY);

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                httpTransport, JSON_FACTORY, clientSecrets, scopes)
                .setDataStoreFactory(dataStoreFactory)
                .build();
        // authorize
        return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
    }

    public static void main(String[] args) throws Exception {

        httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);

        Credential credential = authorize();

        client = new Tasks.Builder(httpTransport, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME).build();

        System.out.println("Connect successful");

        TaskList list = new TaskList();
        list.setTitle("New Task List");
        System.out.println("Creating new task list......");
        TaskList taskList = client.tasklists().insert(list).execute();
        System.out.println("id " + taskList.getId() + "  title " + taskList.getTitle()); //id = MDQ0MTk5NzA5MDI4NDI2MzY0MTA6MTkwMjk3MDcwNDow (for example)
        printTasksFromList(taskList.getId());

        Task task1 = new Task();
        task1.setTitle("task1");
        task1.setNotes("notes for task1");
        System.out.println("Adding task " + task1.getTitle() + ".........");
        client.tasks().insert(taskList.getId(), task1).execute();
        printTasksFromList(taskList.getId());

        Task task2 = new Task();
        task2.setTitle("task2222");
        task2.setNotes("notes for task222");
        System.out.println("Adding task " + task2.getTitle() + ".........");
        Task insertedTask = client.tasks().insert(taskList.getId(), task2).execute();
        printTasksFromList(taskList.getId());

        Task task = client.tasks().get(taskList.getId(), insertedTask.getId()).execute();
        System.out.println(task.toPrettyString());  //по идее, должен быть красивый вывод всех заполненных полей
//        System.out.println(task.toString()); если pretty string будет не очень pretty
        task.setStatus("completed");

        Task updatedTask = client.tasks().update(taskList.getId(), task.getId(), task).execute();
        System.out.println(updatedTask.toPrettyString());

        printTasksFromList(taskList.getId());

        client.tasks().delete(taskList.getId(), updatedTask.getId()).execute();

        printTasksFromList(taskList.getId());


        client.tasks().list(taskList.getId()).clear();
        printTasksFromList(taskList.getId());
    }

    private static void printTasksFromList(String listId) throws IOException {
        System.out.println("Printing tasks from list " + listId + "............");
        List<Task> items = client.tasks().list(listId).setFields("items(id,notes,title)").execute().getItems();
        if (items == null) {
            System.out.println("List is empty");
            return;
        }
        for (Task item : items) {
            System.out.println("id " + item.getId() + "  title " + item.getTitle() + "  notes " + item.getNotes());
        }
        System.out.println("\n");
    }

}
